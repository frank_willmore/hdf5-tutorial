#include <hdf5.h>
#include <stdlib.h>

int main(){

// hid_t H5Fcreate( const char *name, unsigned flags, hid_t fcpl_id, hid_t fapl_id )

    const char *name = "my_file.h5";
    unsigned flags = H5F_ACC_TRUNC;
    hid_t fcpl_id = H5P_DEFAULT;
    hid_t fapl_id = H5P_DEFAULT;
    hid_t file_id = H5Fcreate( name, flags, fcpl_id, fapl_id );

// hid_t H5Screate_simple( int rank, const hsize_t * current_dims, const hsize_t * maximum_dims )

    int rank = 2;  
    const hsize_t current_dims[] = {3,4};
    const hsize_t *maximum_dims = NULL;
    hid_t space_id = H5Screate_simple( rank, current_dims, maximum_dims );

// hid_t H5Gcreate( hid_t loc_id, const char *name, hid_t lcpl_id, hid_t gcpl_id, hid_t gapl_id )

    hid_t loc_id = file_id;
    const char *group_name = "my_group";
    hid_t lcpl_id = H5P_DEFAULT;
    hid_t gcpl_id = H5P_DEFAULT;
    hid_t gapl_id = H5P_DEFAULT;
    hid_t group_id = H5Gcreate( loc_id, group_name, lcpl_id, gcpl_id, gapl_id );

// hid_t H5Dcreate( hid_t loc_id, const char *name, hid_t dtype_id, hid_t space_id, hid_t lcpl_id, hid_t dcpl_id, hid_t dapl_id )
    loc_id = group_id;
    const char *dataset_name = "my_dataset";
    hid_t dtype_id = H5T_NATIVE_INT;
    hid_t dcpl_id = H5P_DEFAULT;
    hid_t dapl_id = H5P_DEFAULT;
    hid_t dataset_id = H5Dcreate( loc_id, dataset_name, dtype_id, space_id, lcpl_id, dcpl_id, dapl_id );

// herr_t H5Dwrite( hid_t dataset_id, hid_t mem_type_id, hid_t mem_space_id, hid_t file_space_id, hid_t xfer_plist_id, const void * buf )
    hid_t mem_type_id = H5T_NATIVE_INT;
    hid_t xfer_plist_id = H5P_DEFAULT;
    const void *buf = malloc(3 * 4 * sizeof(int));
    herr_t write_status = H5Dwrite( dataset_id, mem_type_id, space_id, space_id, xfer_plist_id, buf );

// herr_t H5Xclose(Xid);

    herr_t D_status = H5Dclose(dataset_id);
    herr_t G_status = H5Gclose(group_id);
    herr_t S_status = H5Sclose(space_id);
    herr_t F_status = H5Fclose(file_id);

    return 0;
}

