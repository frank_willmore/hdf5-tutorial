# User Configuration of VirtualBox

* Install the VM.

## Configure networking

Note that by default, there is only one network connection enabled in VirtualBox. This allows the guest to connect to the internet via VirtualBox NAT on the host. It does not allow the host to ssh to the VM. I prefer to treat the VM as if it were another remote host and connect via ssh tools, as I do with any major compute installation. To do this, we 'install' or enable a second network connection:

* Power down the VM.
* Enable the second network adapter.
* Select host-only adapter. 
* Power up the VM.

## find your IP address:

    hdf5-user@HDF5--Tutorial:~$ ifconfig
    enp0s3    Link encap:Ethernet  HWaddr 08:00:27:48:83:5d  
              inet addr:192.168.56.101  Bcast:192.168.56.255  Mask:255.255.255.0
              inet6 addr: fe80::79fb:af5c:3824:8c07/64 Scope:Link
              UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
              RX packets:79 errors:0 dropped:0 overruns:0 frame:0
              TX packets:111 errors:0 dropped:0 overruns:0 carrier:0
              collisions:0 txqueuelen:1000 
              RX bytes:12897 (12.8 KB)  TX bytes:17335 (17.3 KB)

We see that it's 192.168.56.101.

## User your favorite ssh client to connect to the VM from the host:

    host-machine$ ssh hdf5-user@192.168.56.101
    hdf5-user@192.168.56.101's password: 
    Welcome to Ubuntu 16.04.1 LTS (GNU/Linux 4.4.0-31-generic x86_64)

     * Documentation:  https://help.ubuntu.com
     * Management:     https://landscape.canonical.com
     * Support:        https://ubuntu.com/advantage

    369 packages can be updated.
    156 updates are security updates.


    The programs included with the Ubuntu system are free software;
    the exact distribution terms for each program are described in the
    individual files in /usr/share/doc/*/copyright.

    Ubuntu comes with ABSOLUTELY NO WARRANTY, to the extent permitted by
    applicable law.

    hdf5-user@HDF5--Tutorial:~$ 

That's it. You're connected to the VM via your favorite ssh client! 



