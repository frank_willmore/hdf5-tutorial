# Configuring the VM

* Download an .iso and install (I installed Ubuntu 16.04.2-desktop) 

## Configure networking

Note that by default, there is only one network connection enabled in VirtualBox. This allows the guest to connect to the internet via VirtualBox NAT on the host. It does not allow the host to ssh to the VM. I prefer to treat the VM as if it were another remote host and connect via ssh tools, as I do with any major compute installation. To do this, we 'install' or enable a second network connection:

* Power down the VM.
* Enable the second network adapter.
* Select host-only adapter. 
* Power up the VM.

## Make the VM accessible by installing openssh-server:

    $ sudo apt-get install openssh-server

## find your IP address:

    hdf5-user@HDF5--Tutorial:~$ ifconfig
    enp0s3    Link encap:Ethernet  HWaddr 08:00:27:48:83:5d  
              inet addr:192.168.56.101  Bcast:192.168.56.255  Mask:255.255.255.0
              inet6 addr: fe80::79fb:af5c:3824:8c07/64 Scope:Link
              UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
              RX packets:79 errors:0 dropped:0 overruns:0 frame:0
              TX packets:111 errors:0 dropped:0 overruns:0 carrier:0
              collisions:0 txqueuelen:1000 
              RX bytes:12897 (12.8 KB)  TX bytes:17335 (17.3 KB)

We see that it's 192.168.56.101.

## Connect to the VM from the host:

    host-machine$ ssh hdf5-user@192.168.56.101
    hdf5-user@192.168.56.101's password: 
    Welcome to Ubuntu 16.04.1 LTS (GNU/Linux 4.4.0-31-generic x86_64)

     * Documentation:  https://help.ubuntu.com
     * Management:     https://landscape.canonical.com
     * Support:        https://ubuntu.com/advantage

    369 packages can be updated.
    156 updates are security updates.


    The programs included with the Ubuntu system are free software;
    the exact distribution terms for each program are described in the
    individual files in /usr/share/doc/*/copyright.

    Ubuntu comes with ABSOLUTELY NO WARRANTY, to the extent permitted by
    applicable law.

    hdf5-user@HDF5--Tutorial:~$ 

--- 

# Setup of the VM internals

    $ sudo apt install git
    $ ssh-keygen

* Install Anaconda... has jupyter, h5py and all the good stuff.

    $ wget https://repo.continuum.io/archive/Anaconda3-4.3.0-Linux-x86_64.sh
    $ bash Anaconda3-4.3.0-Linux-x86_64.sh

* Add key to repo on bitbucket, and remember to revoke it after you distribute the VM
* Grab a couple packages to make hdf5 build:

    $ apt install autoconf
    $ apt install mpich

* Unpack HDF5, as hdf5-1.9.236 and configure:

    $ ./configure --enable-parallel  --prefix=/usr/local/phdf5/1.9.236  
    $ make -j4
    $ make install

* add the path the hdf5 junk:

    hdf5-user@HDF5--Tutorial:~$ cat >> ~/.bashrc << EOF
    export PATH=/usr/local/phdf5/1.9.236/bin:$PATH
    EOF

* set jupyter configuration so that it will bind to all ports:

    $ jupyter notebook --generate-config 
    $ cat >> ~/.jupyter/jupyter_notebook_config.py << EOF
    # Set ip to '*' to bind on all interfaces (ips) for the public server
    c.NotebookApp.ip = '*'
    EOF

* launch the notebook

    $ jupyter notebook

* Set up for the REST API:

    $ git clone https://github.com/HDFGroup/h5pyd.git
    $ cd h5pyd
    $ python setup.py install


